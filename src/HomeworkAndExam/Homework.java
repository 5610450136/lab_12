package HomeworkAndExam;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class Homework extends JFrame {
	private JFrame frame;
	private JPanel pn;
	private JTextArea txt;
	private InputStream path;
	
	public void createframe() {
		frame = new JFrame();
		frame.setSize(800, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		pn = new JPanel();
		txt = new JTextArea();
		pn.add(txt);
		frame.getContentPane().add(pn);
	}
	
	public void setText(String text) {
		txt.setText(text);
	}
	
	public void setFile(String path) {
		this.path = getClass().getResourceAsStream(path);
	}
	
	public ArrayList<String> getSourceFile() throws IOException {
		ArrayList<String> score = new ArrayList<String>();
		BufferedReader reader = new BufferedReader(new InputStreamReader(path));
		String line = null;
        while((line = reader.readLine()) != null) {
        	ArrayList<String> lst = new ArrayList<String>();
        	for (String part : line.split(", ")) {
        	    lst.add(part);
        	}
        	double sum = 0;
        	int i;
        	for (i = 1;i<lst.size();i++) {
        		sum = sum+Double.parseDouble((String) lst.get(i));
        	}
        	String avg = Double.toString(sum/(i-1));
        	String average = lst.get(0)+"      "+avg;
        	score.add(average);
        }
        reader.close();
		return score;
	}
	
	public void writeFile(ArrayList<String> score) throws IOException {
		BufferedWriter writer;
		File avgfile = new File("src/Average_Homework.txt");
		if(!avgfile.exists()) {
			avgfile.createNewFile();
		}
		
		FileWriter fw = new FileWriter(avgfile);
		writer = new BufferedWriter(fw);
		writer.write("--------- Homework Scores ---------"+"\n");
		writer.write("NAME    SCORE"+"\n");
		writer.write("==================================="+"\n");
		for (String line : score) {
			writer.write(line+"\n");
		}
		writer.flush();
		writer.close();
		fw.close();
	}
	
	public String readFile() throws IOException {
		String ans = "";
		BufferedReader reader = new BufferedReader(new InputStreamReader(path));
		
		String line = null;
        while((line = reader.readLine()) != null) {
            ans = ans + line + "\n";
        }
        reader.close();
		return ans;
	}
}
