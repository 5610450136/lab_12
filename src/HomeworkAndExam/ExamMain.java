package HomeworkAndExam;

import java.io.IOException;

public class ExamMain {

	public static void main(String[] args) throws IOException {
		
		try {
			Exam ex = new Exam();
			
			ex.setFile("homework.txt");
			ex.writeFile(ex.getSourceFile(), "homework");
			
			ex.setFile("exam.txt");
			ex.writeFile(ex.getSourceFile(), "exam");
			
			ex.setFile("../Average_Homework_And_Exam.txt");
			ex.createframe();
			ex.setText(ex.readFile());
			
		}
		catch (Exception e) {
			System.out.println(e);
		}
	}

}
