package PhoneBookList;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class PhoneBook extends JFrame {
	private InputStream path;
	
	private JFrame frame;
	private JTextArea txt;
	private JPanel pn;
	
	public PhoneBook() {
		createframe();
	}
	
	public void createframe() {
		frame = new JFrame();
		frame.setSize(800, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		pn = new JPanel();
		txt = new JTextArea();
		pn.add(txt);
		frame.getContentPane().add(pn);
	}
	
	public void setText(String text) {
		txt.setText(text);
	}
	
	public void setFile(String path) {
		this.path = getClass().getResourceAsStream(path);
	}
	
	public String readFile() throws IOException {
		String ans = "";
		BufferedReader reader = new BufferedReader(new InputStreamReader(path));
		String line = null;
        while((line = reader.readLine())!=null){
            ans = ans + line + "\n";
        }
        reader.close();
		return ans;
	}

}
